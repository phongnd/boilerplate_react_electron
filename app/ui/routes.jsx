import React from 'react';
import {Route} from 'react-router';

import HomePage from './modules/core/containers/Home';

export default (
    <Route>
        <Route path="/" component={HomePage}/>
    </Route>
);