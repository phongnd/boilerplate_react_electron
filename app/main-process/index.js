import {readFile} from 'fs-extra'
const {ipcMain, dialog, app, BrowserWindow} = require('electron');

export default function() {
    ipcMain.on('open-file-dialog', (event) => {
        dialog.showOpenDialog({
            properties: ['openDirectory']
        }, function (files) {
            if (files) event.sender.send('selected-directory', files)
        })
    });

    ipcMain.on('restart-app', () => {
        app.relaunch();
        app.exit(0)
    });

    ipcMain.on('toggle-console', () => {
        // 1 is the main browser window id
        BrowserWindow.fromId(1).webContents.toggleDevTools();
    });
}