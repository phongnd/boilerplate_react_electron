declare module 'gulp-postcss' {
    function postcss(any: any): NodeJS.ReadWriteStream;
    module postcss { }
    export = postcss;
}