const fs = require('fs-extra');
const glob = require('glob');

glob('app/ui/**/*.scss', {}, (error, files) => {
    if (error) throw error;

    let filelist = files.map((file) => {
        let path = file
            .replace('app/ui', '../app/ui')
            .replace('/_', '/')
            .replace('.scss', '');
        return `@import '${path}';`
    }).join('\n');
    const writeFile = new Promise((resolve, reject) => {
        fs.readFile('tools/template/components.scss', 'utf8', (error, templateContent) => {
            if (error) throw error;
            let content = templateContent.replace('/* components:scss *//* endinject */', filelist);
            fs.ensureDirSync('dev');
            fs.writeFile('dev/components.scss', content, 'utf8', (error) => {
                if (error) reject(error);
                resolve();
            })
        })
    });
    writeFile.then(() => {
    }).catch((error) => console.log(error))
});