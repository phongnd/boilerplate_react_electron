const fsbx = require('fuse-box');

const fuseBox = fsbx.FuseBox.init({
    homeDir: 'app/ui',
    sourcemaps: true,
    outFile: './dev/ui.js',
    cache: true,
    serverBundle: true,
    shim: {
        electron: { exports: "global.require('electron')" },
    },
    plugins: [
        fsbx.EnvPlugin({ NODE_ENV: "development" }),
        fsbx.BabelPlugin(),
        fsbx.JSONPlugin(),
        fsbx.HTMLPlugin({ useDefault: false })
    ]
}).bundle(">index.jsx");