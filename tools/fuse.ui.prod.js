const fsbx = require('fuse-box');

const fuseBox = fsbx.FuseBox.init({
    homeDir: 'app/ui',
    outFile: './prod/ui.js',
    serverBundle: true,
    shim: {
        electron: { exports: "global.require('electron')" },
    },
    plugins: [
        fsbx.EnvPlugin({ NODE_ENV: "production" }),
        fsbx.BabelPlugin(),
        fsbx.JSONPlugin(),
        fsbx.HTMLPlugin({ useDefault: false }),
        fsbx.UglifyJSPlugin()
    ]
}).bundle(">index.jsx");