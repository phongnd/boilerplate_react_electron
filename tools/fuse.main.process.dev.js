const spawn = require('child_process').spawn;
const fsbx = require('fuse-box');

const fuseBox = fsbx.FuseBox.init({
    homeDir: 'app',
    outFile: './dev/main.js',
    serverBundle: true,
    shim: {
        electron: { exports: "global.require('electron')" },
    },
    plugins: [
        fsbx.EnvPlugin({ NODE_ENV: "development" }),
        fsbx.BabelPlugin(),
        fsbx.JSONPlugin()
    ]
}).bundle(">main.js", () => {
    spawn('npm', ['run', 'start:electron'], { shell: true, env: process.env, stdio: 'inherit' })
        .on('close', code => process.exit(code))
        .on('error', spawnError => console.error(spawnError));
});